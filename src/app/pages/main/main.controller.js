'use strict';

function MainController($log, Restangular) {
    'ngInject';
    $log.debug('Hello from main controller!');
    this.isLoaded = false;
    var _this = this;

    Restangular.all("users").getList()
        .then(function(users) {
            _this.users = users;
            _this.isLoaded = true;
        });
}

export default MainController;