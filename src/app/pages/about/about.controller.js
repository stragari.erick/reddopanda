'use strict';

import _ from 'lodash/core';
import angularLogo from '_images/angular.png';

function AboutController($log) {
    'ngInject';
    $log.debug('Hello from about controller!');
    this.lodash_version = _.VERSION;
    this.angularLogo = angularLogo;
}

export default AboutController;