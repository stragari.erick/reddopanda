'use strict';

function ProjectController($scope, $log, $stateParams, Restangular) {
    'ngInject';
    $log.debug('Hello from project controller!');
    this.userid = $stateParams.userid;
    this.projects = [];
    this.isLoaded = false;
    this.mesagge = "Loading...";
    var _this = this;
    Restangular.one('users', this.userid).getList('repos')
        .then(function(projects) {
            if (projects.length > 0) {
                _this.projects = projects;
            } else {
                _this.mesagge = "Sorry, user not have projects, go back";
            }
            _this.isLoaded = true;
        });

    this.getDate = function(date) {
        let options = {
            weekday: 'short',
            year: 'numeric',
            month: 'short',
            day: 'numeric',
            hour: '2-digit',
            minute: '2-digit',
            second: '2-digit'
        };
        return (new Date(date)).toLocaleDateString('en-US', options);
    }

}

export default ProjectController;