'use strict';

import projectTpl from './project.html';
import projectController from './project.controller';

function routeConfig($stateProvider) {
    'ngInject';

    $stateProvider
        .state('projects', {
            url: '/git/users/{userid}',
            templateUrl: projectTpl,
            controller: projectController,
            controllerAs: 'project'
        });
}

export default routeConfig;