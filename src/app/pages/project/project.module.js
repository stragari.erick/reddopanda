'use strict';

import route from './project.route';
import './project.scss';

const projectPageModule = angular.module('project-module', [
    'ui.router'
]);

projectPageModule
    .config(route);

export default projectPageModule;