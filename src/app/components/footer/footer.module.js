'use strict';

import footerDirective from './footer.directive';
import './footer.scss';

const footerModule = angular.module('footer-module', []);

footerModule
    .directive('footerReddo', footerDirective);

export default footerModule;