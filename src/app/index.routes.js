'use strict';



function routeConfig($urlRouterProvider, RestangularProvider) {
    'ngInject';


    // set URL base in ReddoPanda
    $urlRouterProvider.otherwise('/git/users/');

    // set Git RestAPI
    RestangularProvider.setBaseUrl('https://api.github.com');

}

export default angular
    .module('index.routes', [])
    .config(routeConfig);