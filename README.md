## ReddoPanda


## Synopsis

This is a simple angular webpack es6 use, compatible with lazy-loading modules.

It was generated using Yeoman "https://github.com/STUkh/generator-angular-webpack-es6

## Installation

First of all, I recomend to use Ubuntu to make sure that all nmp modules are working well and beacuse that this application was develop on Ubuntu.

Install git, curl, make and gcc, nodejs, npm, yeoman and the generator generator-angular-webpack-es6

Download the project in to a directory

getin into directory ReddoPanda

                     $cd reddopanda

Install all npm modules:

                    $sudo npm install

this will ask your password, it take time to install all npm dependences.

When the npm nodules finish to install run the aplication:

                     $npm start

It will launch the webpack actions when it finished it will open the browser in the next link: http://localhost:8080/#!/git/users/ 



## Code Example

This angular application take advantage of angular module, that it allow develop modules to big web applications.

### Directoy structure

ReddoPanda
|   config

│   ├── manifest.json

│   └── webpack

│       ├── environments

│       │   ├── development.js

│       │   └── production.js

│       └── global.js

├── package.json

├── postcss.config.js

├── src

│   ├── app

│   │   ├── components

│   │   │   ├── footer

│   │   │   │   ├── footer.directive.js

│   │   │   │   ├── footer.html

│   │   │   │   ├── footer.module.js

│   │   │   │   └── footer.scss

│   │   │   └── header

│   │   │       ├── header.directive.js

│   │   │       ├── header.html

│   │   │       ├── header.module.js

│   │   │       └── header.scss

│   │   ├── core

│   │   │   ├── core.module.js

│   │   │   ├── directives

│   │   │   │   └── validation-test

│   │   │   │       └── validation-test.directive.js

│   │   │   └── services

│   │   │       ├── constants.js

│   │   │       ├── resolver.provider.js

│   │   │       └── store.factory.js

│   │   ├── index.bootstrap.js

│   │   ├── index.components.js

│   │   ├── index.config.js

│   │   ├── index.module.js

│   │   ├── index.routes.js

│   │   ├── index.run.js

│   │   ├── index.vendor.js

│   │   └── pages

│   │       ├── about

│   │       │   ├── about.controller.js

│   │       │   ├── about.html

│   │       │   ├── about.module.js

│   │       │   ├── about.route.js

│   │       │   └── about.scss

│   │       ├── main
│   │       │   ├── main.controller.js

│   │       │   ├── main.html

│   │       │   ├── main.module.js

│   │       │   ├── main.route.js

│   │       │   └── main.scss

│   │       └── project

│   │           ├── project.controller.js

│   │           ├── project.html

│   │           ├── project.module.js

│   │           ├── project.route.js

│   │           └── project.scss

│   ├── assets

│   │   ├── images

│   │   │   ├── angular.png

│   │   │   └── yeoman.png

│   │   ├── js

│   │   └── styles

│   │       └── sass

│   │           └── index.scss

│   ├── favicon.ico

│   └── tpl-index.ejs

└── webpack.config.js


The principal file is index.bootstrap tahat load all modules in the application.

This little application use:

Angular 1.6.4 

Bootstrap 3.3.7

Ui-Route: it make easy the navegation among internals views

Restangular: Service to make the calls to Git Rest API

Lodash: to give supprtu to restangular.

Webpack 2: to load the angular app and open a web browser automatically with the link http://localhost:8080/


## Feature

Load and display git users in cards.

Link to user git page.

Link to internal page to display the user projects.

Display projects of a user in cards.

Link to user project.

Display a loading view.


## Missing task:

Pagination un both pages (user and project)

A Button to load more users

## Improvements to do:

Use ui-pagination to display the pagination un both pages (user and project)

Add load more items user using vertical scrollbar instead of a button to load more users

Move the loading view to a directive to use in any internal page 

Create a card-directive to display some informations in a cards 

Change the favicon to custom favicon 

Use the Restangular into a service 

Add restangular objects to encapsulate behavior, for example for user retrieve more information like number of followers or number of projects

Add unit test to components 

Add a server to allow more posible features like login or save the state 

Improve css acconding to scss style 

